﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallSpawnController : MonoBehaviour {
    [SerializeField]
    MyGameManager myManager;
    public GameObject[] arrayBolas;
    [SerializeField]
    GameObject goParticles;
    public float fSpeed;
    public Transform[] transformPuntoOrigen;
    public Transform transformPuntoMedio;
    public Transform[] transformPuntoDestino;

//    public float fTimer = 3f;
//    public int timeLeft = 3;
    
    private MoveToDestination moveBola;
    private GameObject goTemp;
    private GameObject goToDestroy;
    private bool bFlag = false;

    int iRandomOriginIndex;

    public string getActiveBall()
    {
        return goTemp.tag;
    }

//    public float GetTimer(){
//        return fTimer;
//    }

    public void NewBall()
    {
        iRandomOriginIndex = Random.Range(0,transformPuntoOrigen.Length);
//        Debug.Log("Creando bola...");
        goTemp = Instantiate(arrayBolas[Random.Range(0, arrayBolas.Length)]);
        goTemp.transform.position = transformPuntoOrigen[iRandomOriginIndex].position;
        goTemp.transform.rotation = Random.rotation;

        moveBola = goTemp.GetComponent<MoveToDestination>();
        
//        moveBola = arrayBolas[i].GetComponent<MoveToDestination>();
        moveBola.setDestino(transformPuntoMedio.position);
        moveBola.setSpeed(fSpeed);
    }

    public void ballToDestiny()
    {
        DestroyLastBall();
        NewBall();
    }

    public void DestroyLastBall(){
        bFlag = true;
//        moveBola.setDestino(transformPuntoDestino.position);
        moveBola.setDestino(transformPuntoDestino[iRandomOriginIndex].position);
        moveBola.setMove(true);
        
        //        moveBola.getArrived() 
        goToDestroy = goTemp;
    }

    void Update(){
        if(myManager.GetGamePaused() == false){
            moveBola.setMove(true);
            if ((moveBola.getArrived() == true) && bFlag == true)
            {
                Destroy(goToDestroy);
                bFlag = false;
            }
        }
    }
}
