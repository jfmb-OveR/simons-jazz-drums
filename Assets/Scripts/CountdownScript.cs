﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CountdownScript : MonoBehaviour
{
    public SceneController mySceneController;


    private void GoToStage()
    {
        Debug.Log("Stage: " + mySceneController.GetStage());
        mySceneController.StartGame();
        switch(mySceneController.GetStage()){
            case 0:
                Stage0();
                break;
            case 1:
                Stage1();
                break;
            case 2:
                Stage2();
                break;
            case 3:
                Stage3();
                break;
            case 4:
                Stage4();
                break;
            case 5:
                Stage5();
                break;
        }
    }

    private void Stage0(){
            mySceneController.Stage0();
    }

    public void Stage1(){
        mySceneController.Stage1();
    }
    public void Stage2(){
        mySceneController.Stage2();
    }
    public void Stage3(){
        mySceneController.Stage3();
    }
    public void Stage4(){
        mySceneController.Stage4();
    }
    public void Stage5(){
        mySceneController.Stage5();
    }
}
