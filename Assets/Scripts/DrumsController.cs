﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrumsController : MonoBehaviour
{
    [SerializeField]
    GameObject goParticlesPrefab;

    [SerializeField]
    GameObject[] arrayLoops;
    [SerializeField]
    GameObject[] arrayPlates;
    // Start is called before the first frame update
    [SerializeField]
    GameObject[] arrayCoolPlates;
    [SerializeField]
    GameObject[] arrayBestDrums;

    [SerializeField]
    CameraShake cameraShake;
    
    [SerializeField]
    AudioSource audioAmbience;
    [SerializeField]
    AudioSource audioBass;
    [SerializeField]
    AudioSource audioPiano;

    [SerializeField]
    AudioSource audioTrumpet;

    [SerializeField]
    AudioSource audioAplausos;
    [SerializeField]
    AudioSource audioBooo;
    

    private int iLastIndex;
    private bool bPlayAmbience = false;
    private float fAmbienceVolume = 0f;

    //Called by SceneController
    public void StartGameObjects(){
        ResetGameObjects(arrayLoops, false);
    }

    public void ResetGameObjects(GameObject[] goObjects, bool bNew){
        for (int i = 0; i < goObjects.Length; i++){
            goObjects[i].SetActive(bNew);
        }
    }

    public void PlayAplausos(){
        audioAplausos.Play();
    }

    public void PlayBooo(){
        audioBooo.Play();
    }

    public void PlayAmbience(){
        bPlayAmbience = true;
        audioAmbience.Play();
    }

    public void StopPlayAmbience(){
        bPlayAmbience = false;
    }
/*
    public void PlaySound(GameObject[] goSounds){
        int iIndex = 0;
        do{
            iIndex = Random.Range(0,goSounds.Length);
        }while(iIndex == iLastIndex);
        iLastIndex = iIndex;

//        Debug.Log("SOnido número: " + iIndex);

        goSounds[iIndex].SetActive(true);
        StartCoroutine(CorSoundDisabled(goSounds, iIndex));
    }
*/

    public void PlayRandomSound(GameObject[] arrayGO){
        int iIndex= Random.Range(0,arrayGO.Length);

        GameObject goNewSound = Instantiate(arrayGO[iIndex], this.gameObject.transform);
        StartCoroutine(CorDestroyObject(goNewSound, 2f));        
    }

/*
    public void PlayRandomPlate(){
        PlaySound(arrayPlates);
    }
*/

    public void PlayRandomPlate(){
        PlayRandomSound(arrayPlates);
    }

    public void PlayRandomCoolPlate(){
//        GameObject goNewParticles = Instantiate(goParticlesPrefab, new Vector3(0, 0, 0), Quaternion.identity);
//        StartCoroutine(CorDisableParticles(goNewParticles));
        cameraShake.EnableShake(0.2f);
        #if UNITY_ANDROID
            Handheld.Vibrate();
        #endif
        PlayRandomSound(arrayCoolPlates);        
    }

    public void PlayRandomBestDrums(){
        PlayRandomSound(arrayBestDrums);
    }

    public void StartParticles(){
        GameObject goNewParticles = Instantiate(goParticlesPrefab, new Vector3(0, 0f, 0), Quaternion.identity);
//        StartCoroutine(CorDisableParticles(goNewParticles));
        StartCoroutine(CorDestroyObject(goNewParticles, 3f));
    }

    public void PlayLoop(int iIndex){
        Debug.Log("Loop index: " + iIndex);
        if(iIndex < arrayLoops.Length){
            arrayLoops[iIndex].SetActive(true); 
            if(iIndex > 0)
                arrayLoops[iIndex-1].SetActive(false);            
        }
    }

    public void StopAllLoops()
    {
        for (int i = 0 ; i < arrayLoops.Length; i++)
            arrayLoops[i].SetActive(false);
    }

/*
    IEnumerator CorSoundDisabled(GameObject[] goArray, int iIndex){
        yield return new WaitForSeconds(2f);
        goArray[iIndex].SetActive(false);
//        ResetGameObjects();
    }
*/
   
    IEnumerator CorDestroyObject(GameObject goObject, float fSeconds){
        yield return new WaitForSeconds(fSeconds);
        Destroy(goObject);
    }
}
