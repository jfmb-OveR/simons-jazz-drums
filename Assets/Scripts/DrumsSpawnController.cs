﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrumsSpawnController : MonoBehaviour
{
    [SerializeField]
    Transform[] arrayGoPositions;

    [SerializeField]
    GameObject[] arrayGoDrums;

    private bool[] arrayPositionsAvailable;
    private int iPositionsAvailableCounter = 0;

    
    private void ResetPositions(){
        for(int i = 0; i < arrayPositionsAvailable.Length; i++){
            arrayPositionsAvailable[i] = true;
        }
        iPositionsAvailableCounter = arrayPositionsAvailable.Length;
    }

    private void NewPositions(){
        int iNewPosition = 0;

        for(int i = 0; i < arrayPositionsAvailable.Length; i++){
//            if(iPositionsAvailableCounter > 0){
                do{
                    iNewPosition = Random.Range(0,arrayPositionsAvailable.Length);
                }while (arrayPositionsAvailable[iNewPosition] == false);
                
                arrayPositionsAvailable[iNewPosition] = false;
                arrayGoDrums[i].transform.position = new Vector3(arrayGoPositions[iNewPosition].position.x, arrayGoPositions[iNewPosition].position.y, arrayGoPositions[iNewPosition].position.z);

                arrayGoDrums[i].transform.rotation = Random.rotation;

                iPositionsAvailableCounter--;
//            }
        }

//        Debug.Log("Reordenación terminada!!!!");
    }

    public void ChooseNewPositions(){
        ResetPositions();

        NewPositions();
    }

    void Start(){
        arrayPositionsAvailable = new bool[arrayGoPositions.Length];

        ChooseNewPositions();
    }
}
