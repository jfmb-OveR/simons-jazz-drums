﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroScript : MonoBehaviour
{
    [SerializeField]
    GameObject goMainGameObjects;
    public void EnableMainGameObjects(){
        this.transform.gameObject.SetActive(false);
        goMainGameObjects.SetActive(true);
    }

}
