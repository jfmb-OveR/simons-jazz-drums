﻿
//public class MoveToDestination : MonoBehaviour {
using UnityEngine;
using System.Collections;

public class MoveToDestination : MonoBehaviour
{

    private float fSpeed;
    private Vector3 vDestino;
    private bool bMove;
    private bool bArrived;

    private bool bKeepObject = true;
    public bool getArrived()
    {
        return bArrived;
    }

    public void setMove(bool newValue)
    {
        bMove = newValue;
    }

    public void setDestino(Vector3 newDest)
    {
        vDestino = newDest;
    }

    public void setSpeed(float newValue)
    {
        fSpeed = newValue;
    }

    public void setKeepObject(bool newValue)
    {
        bKeepObject = newValue;
    }

    // Use this for initialization
    void Start()
    {
        bArrived = false;
        bMove = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (bMove == true)
        {
            if (vDestino != null)
            {
                this.transform.position = Vector3.MoveTowards(transform.position, vDestino, fSpeed * Time.deltaTime);
                if (transform.position == vDestino)
                {
                    bMove = false;
                    bArrived = true;
//                    this.GetComponent<Collider2D>().enabled = false;
                    if(bKeepObject == false)
                    {
                        Destroy(this.gameObject);
                    }
                }
            }
        }
    }
}
