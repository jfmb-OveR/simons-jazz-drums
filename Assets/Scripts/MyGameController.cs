using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyGameController : MonoBehaviour
{
    bool bGamePaused = false;
    bool bGameOver = true;

    public bool GetGamePaused(){
        return bGamePaused;
    }

    public bool GetGameOver(){
        return bGameOver;
    }

    public void SetGamePaused(bool bNewValue){
        bGamePaused = bNewValue;
    }

    public void SetGameOver(bool bNewValue){
        bGameOver = bNewValue;
    }


    public void SetGameStart(){
        SetGamePaused(false);
        SetGameOver(false);
    }

    public void SetGamePaused(){
        SetGamePaused(true);
    }

    public void SetGameOver(){
        SetGamePaused(true);
        SetGameOver(true);
    }
}
