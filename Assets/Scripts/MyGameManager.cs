﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour
{
    [SerializeField]
    GameObject goMainObjects;
    [SerializeField]
    GameObject goIntro;
    [SerializeField]
    GameObject goFinalScreen;
    [SerializeField]
    Text textFinalScore;
    [SerializeField]
    Text textBestScoreQuote;

    [SerializeField]
    UnityAds myAdsManager;

    bool bGamePaused = false;
    bool bGameOver = true;
    
    int iFinalScore = 0;

    private int iLastStage = 0;

    private bool bAdEnded = false;

    public int GetScore(){
        return iFinalScore;
    }

    public int GetLastStage(){
        return iLastStage;
    }

    public void SetLastStage(int iNewValue){
        iLastStage = iNewValue;
    }

#region PlayerPrefs
    public int GetPlayerPrefInt(string sValue){
        return PlayerPrefs.GetInt(sValue);
    }

    public void SetPlayerPrefInt(string sValue, int iValue){
        PlayerPrefs.SetInt(sValue, iValue);
    }
#endregion

    public void ContinueGameWithCurrentScore(){
        bAdEnded = false;
        int iScoreTemp = iFinalScore;
        StartCoroutine(CorShowVideo(iScoreTemp));
    }

    IEnumerator CorShowVideo(int iScoreTemp){
        myAdsManager.ShowVideo();
        yield return new WaitUntil(() => myAdsManager.VideoFinished() == true);
        ShowHideFinalScore(false);
        yield return new WaitForSeconds(1f);
        SetGameStart(iScoreTemp);
        bAdEnded = true;
    }

    public bool ContinueAfterAdEnded(){
        return bAdEnded;
    }

    public bool GetGamePaused(){
        return bGamePaused;
    }

    public bool GetGameOver(){
        return bGameOver;
    }

    public void SetFinalScore(int iScore){
        iFinalScore = iScore;
        Debug.Log("Final Score: " + iFinalScore +" Best Result: " +GetPlayerPrefInt("PPBestScore"));

        if(iFinalScore > GetPlayerPrefInt("PPBestScore")){
            Debug.Log("Final Score: " + iFinalScore +" Best Result: " +GetPlayerPrefInt("PPBestScore"));
            SetPlayerPrefInt("PPBestScore", iFinalScore);
            textBestScoreQuote.text= "Your best result! But you still have to improve...";

            Debug.Log("Actualizar el Leaderboard de Google Play");
        }
        else{            
            textBestScoreQuote.text= "There are no two words in the English language more harmful than 'good job'.";
        }

        textFinalScore.text = iScore.ToString();
    }

    private void SetGamePaused(bool bNewValue){
        bGamePaused = bNewValue;
    }

    private void SetGameOver(bool bNewValue){     
         bGameOver = bNewValue;
    }

    public void SetGameStart(int iScore){
        SetFinalScore(iScore);
        ShowHideFinalScore(false);
        SetGamePaused(false);
        SetGameOver(false);
    }

    public void SetGamePaused(){
        SetGamePaused(true);
    }

    public void SetGameOver(){
        ShowHideFinalScore(true);
        SetGamePaused(true);
        SetGameOver(true);
    }

    public void ShowHideFinalScore(bool bNewValue){       
        goFinalScreen.SetActive(bNewValue);
    }

    void Awake(){
        ShowHideFinalScore(false);
        goMainObjects.SetActive(false);
        goIntro.SetActive(true);
    }
}
