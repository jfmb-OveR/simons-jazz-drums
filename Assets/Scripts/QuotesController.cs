﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuotesController : MonoBehaviour
{
    [SerializeField]
    Text textQuote;
    [SerializeField]
    string[] arrayBadQuotes;
    [SerializeField]
    string[] arrayGoodQuotes;

    public void ShowQuote(string[] arrayQuotes){
        textQuote.text = arrayQuotes[Random.Range(0,arrayQuotes.Length)];
        textQuote.enabled = true;

        StartCoroutine(CorHideQuote());
    }

    public void ShowBadQuote(){
        ShowQuote(arrayBadQuotes);
    }

    public void ShowGoodQuote(){
        ShowQuote(arrayGoodQuotes);
    }
    IEnumerator CorHideQuote(){
        yield return new WaitForSeconds(3f);
        textQuote.enabled = false;
    }

    void Start()
    {
        textQuote.enabled = false;
    }
}
