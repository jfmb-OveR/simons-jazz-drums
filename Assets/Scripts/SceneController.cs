﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    [SerializeField]
    MyGameManager myManager;
    [SerializeField]
    BallSpawnController myObjectsController;
    [SerializeField]
    DrumsSpawnController myDrumsSpawnController;
    [SerializeField]
    DrumsController myDrumsController;
    [SerializeField]
    QuotesController myQuotesController;
    [SerializeField]
    TrackMixer myMixer;

    [SerializeField]
    GameObject[] arrayGoCountdowns;
    [SerializeField]
    int[] arrayIntScores;

    [SerializeField]
    GameObject goWhiplash;

    [SerializeField]
    GameObject goStartButton;

//    [SerializeField]
//    GameObject goTimeWhiteBar;

    [SerializeField]
    public  float fMaxTimerAmount = 3f;

    [SerializeField]
    Transform tBlackSphere;

    [SerializeField]
    GameObject goImageBestHit;

    Image imageWhiteBar;
    public Text textScore;

    private float fMinAmountForBestHit = 0.15f;
    private float fMaxAmountForBestHit = 0.4f;
    private float fMainTimer;

    private float fImageWhiteBarAmount = 0f;
    private float fMetronomAmount = 0;
    private float fMetronomBarAmount01 = 0f;
    private float fMetronomBarAmount02 = 0f;


    private bool bPingPong = true;

    private bool bHit = false;
    private bool bWrongHit = false;

    private int iGoodHits = 0;

    private int iScore ;
    private int iStage = 0;
    private bool[] arrayStages;

    private float fMetronomonSpeed = 1f;
    private int iLoop = 0;
    private int iMinGoodHitsForWhiplashMode = 5;

#region getters

    public float GetMetronomAmount01(){
        return fMetronomBarAmount01;
    }

    public float GetMetronomAmount02(){
        return fMetronomBarAmount02;
    }

    public bool GetHit(){
        return bHit;
    }

    public bool GetWrongHit(){
        return bWrongHit;
    }

    public int GetGoodHits(){
        return iGoodHits;
    }

    public int GetStage(){
        return iStage;
    }
#endregion
    
#region setters    
    public void SetMaxTimerAmount(float fNewValue){
        fMaxTimerAmount = fNewValue;
    }

    public void SetWrongHit(bool bNewValue){
        bWrongHit = bNewValue;
    }

    public void SetHit(bool bNewValue){
        bHit = bNewValue;
    }

    public void SetScore(int iNewValue){
        iScore = iNewValue;
    }

    public void SetStage(int iNewValue){
        iStage = iNewValue;
    }
#endregion


    public void GoodHit(){ //Correct ball hit in the best moment
        SetHit(true);

        myDrumsController.StartParticles();
        iGoodHits++;

        if(GetGoodHits() < iMinGoodHitsForWhiplashMode){
            myDrumsController.PlayRandomBestDrums();
            IncreaseScore(100);
        }
        else{
            myQuotesController.ShowGoodQuote();
            myDrumsController.PlayRandomCoolPlate();
            IncreaseScore(500);
        }

        CheckScore();
    }

    public void WrongHit(){ //Correct ball hit but not in the best moment
        myDrumsController.PlayRandomPlate();
        SetWrongHit(true);
        IncreaseScore(50);
        iGoodHits = 0;

        CheckScore();
    }

    public void ErrorHit(){
        myDrumsController.PlayBooo();
        IncreaseScore(-50);
        iGoodHits = 0;
    }

    void RefreshScore(){
        textScore.text = iScore.ToString();
    }

    public void IncreaseScore(int iNewValue){
        iScore += iNewValue;
        RefreshScore();
        if(iNewValue > 0 && iScore > arrayIntScores[arrayIntScores.Length-1])
            myDrumsSpawnController.ChooseNewPositions();
    }

    public void IncreaseTimer(){
        fMainTimer += 1f;
        if(fMainTimer > fMaxTimerAmount)
            fMainTimer = fMaxTimerAmount;
    }

    public void BallClicked(){
        if(bPingPong == true){
            if(GetMetronomAmount01() >= fMinAmountForBestHit && GetMetronomAmount01() < fMaxAmountForBestHit)
                GoodHit();
            else
                WrongHit();
        }
        else{
            if(GetMetronomAmount02() >= fMinAmountForBestHit && GetMetronomAmount02() < fMaxAmountForBestHit)
                GoodHit();
            else
                WrongHit();
        }

        IncreaseTimer();
    }

    public void HideScore(){
        myManager.ShowHideFinalScore(false);
        ResetGame();
    }

    public void ContinueWithCurrentScore(){ //Referenced by "YES" button
        StartCoroutine(CorContinueWithCurrentScore());
    }

    IEnumerator CorContinueWithCurrentScore(){
        myManager.ContinueGameWithCurrentScore();
        yield return new WaitUntil(() => myManager.ContinueAfterAdEnded() == true);

        int iCurrentStage = myManager.GetLastStage();

        HideScore();

        SetScore(myManager.GetScore());
        GoGame(iCurrentStage);
        
        for (int i = 0; i < arrayStages.Length; i++){
            if(i < iCurrentStage)
                arrayStages[i] = true;
        }
    }

    public void Init(){ //Referenced by Start Button
        SetScore(0);
        GoGame(0);
    }

    public void GoGame(int iNewStage){
        SetStage(iNewStage);

        textScore.enabled = false;
        RefreshScore();
        goStartButton.SetActive(false);

        if(iNewStage < arrayGoCountdowns.Length)
            arrayGoCountdowns[iNewStage].SetActive(true);
        else
            arrayGoCountdowns[arrayGoCountdowns.Length - 1].SetActive(true);    
    }

    public void StartGame(){         //Referenced by CountdownScript   
        textScore.enabled = true;

        myManager.SetGameStart(0);
    }

    public void EndGame(){
        myDrumsController.StopAllLoops();
        myManager.SetGameOver();
    }

#region stages
    public void Stage0(){ //Warm up!
        SetMaxTimerAmount(3f);
        myDrumsController.PlayLoop(iStage);
        fMetronomonSpeed = 1f;

    }
    public void Stage1(){
        SetMaxTimerAmount(3f);
        myDrumsSpawnController.ChooseNewPositions();
        myDrumsController.PlayLoop(iStage);
        fMetronomonSpeed = 1f;
    }

    public void Stage2(){
        SetMaxTimerAmount(2f);

        myDrumsSpawnController.ChooseNewPositions();
        myDrumsController.PlayLoop(iStage);

        fMetronomonSpeed = 1.33f;
    }

    public void Stage3(){
        SetMaxTimerAmount(1.5f);
        myDrumsSpawnController.ChooseNewPositions();
        myDrumsController.PlayLoop(iStage);

        fMetronomonSpeed = 2f;
    }

    public void Stage4(){
        SetMaxTimerAmount(1f);

        myDrumsSpawnController.ChooseNewPositions();
        myDrumsController.PlayLoop(iStage);

        fMetronomonSpeed = 2f;
    }

    public void Stage5(){
        myDrumsSpawnController.ChooseNewPositions();
        myDrumsController.PlayLoop(iStage);

        fMetronomonSpeed = 2f;
    }

#endregion

    private void EnableDisableCountdown(int iIndex, bool bValue){
        if(iIndex < arrayGoCountdowns.Length)
            arrayGoCountdowns[iIndex].SetActive(bValue);
    }

    public void CheckScore(){
        if(iScore > arrayIntScores[0] && arrayStages[0] == false){
            arrayStages[iStage] = true;
            EnableDisableCountdown(iStage, false);
            iStage++;//1
            EnableDisableCountdown(iStage, true);
        }
        if(iScore > arrayIntScores[1] && arrayStages[1] == false){
            arrayStages[iStage] = true;
            EnableDisableCountdown(iStage, false);
            iStage++;//2
            EnableDisableCountdown(iStage, true);
        }
        if(iScore > arrayIntScores[2] && arrayStages[2] == false){
            arrayStages[iStage] = true;
            EnableDisableCountdown(iStage, false);
            iStage++;//3
            EnableDisableCountdown(iStage, true);
        }
/*        
        if(iScore > arrayIntScores[3] && arrayStages[3] == false){
            arrayStages[iStage] = true;
            EnableDisableCountdown(iStage, false);
            iStage++;//4
            EnableDisableCountdown(iStage, true);
        }
        if(iScore > arrayIntScores[4] && arrayStages[4] == false){
            arrayStages[iStage] = true;
            EnableDisableCountdown(iStage, false);
            iStage++;//5
            EnableDisableCountdown(iStage, true);
        }
        */
    }

    public void CheckMetronom(){
        if(fMetronomAmount <= 0){ 
            SetWrongHit(false);
            SetHit(false);

            bPingPong = true;
        }

        if(fMetronomAmount >= 1){
            SetWrongHit(false);
            SetHit(false);

            bPingPong = false;                
        }

        float fTemp = 0;

        if(bPingPong == true){ //Metrónomo 1
            fMetronomAmount += Time.deltaTime * fMetronomonSpeed;
            fTemp = fMetronomAmount;
        }
        else{ //Metrónomo 2
            fMetronomAmount -= Time.deltaTime * fMetronomonSpeed;
            fTemp = 1 - fMetronomAmount;
        } 

        //Si el tiempo es mayor que fMin y menor que fMax, significa que es el momento correcto y se muestra la imagen
        if(fTemp > fMinAmountForBestHit && fTemp < fMaxAmountForBestHit){
            goImageBestHit.SetActive(true);
        }
        else{
            goImageBestHit.SetActive(false);
        }


        if(GetWrongHit() == false && GetHit() == false){ 
            fMetronomBarAmount01 = fMetronomAmount;
            fMetronomBarAmount02 = 1 - fMetronomAmount;
        }
    }

    void ResetStages(){
        iScore = iStage = 0;

        for(int i = 0; i < arrayStages.Length; i++)
            arrayStages[i] = false;
    }

    private void ResetBars(){ //Bars deleted. Now it works with float variables.
        SetWrongHit(false);
        
        fImageWhiteBarAmount = 0f;

        iGoodHits = 0;
    }

    void ResetGame(){
        myMixer.Reset();

        ResetStages();

        myDrumsController.StartGameObjects();

        tBlackSphere.transform.localScale = new Vector3(0f,0f,0f);

        goWhiplash.SetActive(false);
  
        for(int i = 0; i < arrayGoCountdowns.Length; i++){
            arrayGoCountdowns[i].SetActive(false);
        }

        myDrumsController.StopAllLoops();

        ResetBars();

        goImageBestHit.SetActive(false);
        goStartButton.SetActive(true);

        textScore.enabled = false;
      
        SetMaxTimerAmount(3f);
        fMainTimer = fMaxTimerAmount;

        iLoop = 0;
        fMetronomonSpeed = 1f;

        myManager.SetGamePaused();
        myObjectsController.NewBall();   
    }

    IEnumerator CorDisableObject(GameObject goToDisable){
        yield return new WaitForSeconds(3f);
    }

    void Start(){     
        arrayStages = new bool[arrayGoCountdowns.Length];
        ResetGame();
        RefreshScore();
    }

    // Update is called once per frame
    void Update()
    {
        if(myManager.GetGameOver() == false){
            fMainTimer -= Time.deltaTime;
            fImageWhiteBarAmount = fMainTimer / fMaxTimerAmount;

            tBlackSphere.transform.localScale = new Vector3(1-fImageWhiteBarAmount, 1-fImageWhiteBarAmount, 1-fImageWhiteBarAmount);
            CheckMetronom();

            goWhiplash.SetActive(GetGoodHits() >= iMinGoodHitsForWhiplashMode);
            
             if (GetGoodHits() == iMinGoodHitsForWhiplashMode)
                myDrumsController.PlayAplausos();

            if (fMainTimer <= 0){
                myManager.SetFinalScore(iScore);
                myManager.SetLastStage(iStage);
                EndGame();
                myObjectsController.DestroyLastBall();
            }
        }
    }
}
