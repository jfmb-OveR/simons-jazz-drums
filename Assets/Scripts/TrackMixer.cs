﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackMixer : MonoBehaviour
{
    [SerializeField]
    float fTrackTime;
    [SerializeField]
    GameObject[] arrayTracks;
    float fTimer = 0f;
    int iCounter = 0;

    float fMaxTrackLength = 0;

    public void Reset(){
        fTimer = 0;
        iCounter = 0;

        fMaxTrackLength = arrayTracks[iCounter].GetComponent<AudioSource>().clip.length;

        for(int i = 1; i < arrayTracks.Length; i++){
            arrayTracks[i].SetActive(false);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
//        Reset();
    }

    // Update is called once per frame
    void Update()
    {
        fTimer += Time.deltaTime;

        if(fTimer >= fMaxTrackLength){
            iCounter++;
            if(iCounter < arrayTracks.Length){
                arrayTracks[iCounter].SetActive(true);

                if(arrayTracks[iCounter].GetComponent<AudioSource>().clip.length > fMaxTrackLength)
                    fMaxTrackLength = arrayTracks[iCounter].GetComponent<AudioSource>().clip.length;
            }
            fTimer = 0;
        }
    }
}
