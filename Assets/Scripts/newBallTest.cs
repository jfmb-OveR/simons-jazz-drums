﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class newBallTest : MonoBehaviour
{
    public SceneController mySceneController;
    public BallSpawnController myObjectsController;
    public DrumsController myDrumsController;
    [SerializeField]
    MyGameManager myManager;
    
    [SerializeField]
    QuotesController myQuotesController;
    [SerializeField]
    Color colorWhenPressed;
    [SerializeField]

    Color colorWheNotPressed;

    Vector3 vScaleChange;

    public void ResetSize(){
        this.transform.localScale = new Vector3(1f, 1f, 1f);
    }

    void OnMouseDown()
    {
//        this.gameObject.GetComponent<Renderer>().material.color = colorWhenPressed;

        if(myManager.GetGamePaused() == false){
            if (myObjectsController.getActiveBall() == this.tag){
                mySceneController.BallClicked();
                myObjectsController.ballToDestiny();

                this.gameObject.transform.localScale -= vScaleChange;

//                myAnimator.SetBool(sAnimString, true);
            }
            else{
//                Debug.Log("Esfera equivocada");
                myQuotesController.ShowBadQuote();
                mySceneController.ErrorHit();
//                mySceneController.IncreaseScore(-50);
            }
        }
        else{
            ResetSize();
            myDrumsController.PlayRandomCoolPlate();

        }
    }

    void OnMouseUp(){
        ResetSize();
//        this.gameObject.GetComponent<Renderer>().material.color = colorWheNotPressed;
    }

    void Start(){
        vScaleChange = new Vector3(0.2f, 0.2f, 0.2f);
//        colorWheNotPressed = this.gameObject.GetComponent<Renderer>().material.color;
    }
}

